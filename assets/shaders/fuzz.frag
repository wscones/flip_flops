//#version 130

//#ifdef GL_ES
//    precision mediump float;
//#endif

varying vec2 vTexCoord;
uniform sampler2D uImage0;

float rand(vec2 co);
float scaleClamp(float value, float min, float max, float min2, float max2);



void main(void)
{
	float randNum = scaleClamp(rand(vTexCoord), 0.0, 100.0, 0.0, 0.1);
	vec2 randOffset = vec2(randNum, randNum);
	vec4 color = texture2D(uImage0, vTexCoord + randOffset);
	
	vec2 inverted = vec2(1.0 - vTexCoord.x, 1.0 - vTexCoord.y);
	vec4 color2 = texture2D(uImage0, inverted);

	
	vec4 mixed = mix(color, color2, 0.5);
    
    gl_FragColor = vec4(color.r, color.g, color.b, color.a);
}



float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

float scaleClamp(float value, float min, float max, float min2, float max2)
{
	value = min2 + ((value - min) / (max - min)) * (max2 - min2);
	if (max2 > min2)
	{
		value = value < max2 ? value : max2;
		return value > min2 ? value : min2;
	}
	value = value < min2 ? value : min2;
	return value > max2 ? value : max2;
}