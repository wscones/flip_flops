//#version 130

//#ifdef GL_ES
//    precision mediump float;
//#endif

varying vec2 vTexCoord;
uniform sampler2D uImage0;
uniform float theta;

float rand(vec2 co);
float scaleClamp(float value, float min, float max, float min2, float max2);



void main(void)
{
	float cosTheta = cos(theta);
	float sinTheta = sin(theta);
	float newX = (cosTheta * (vTexCoord.x - 0.5) - sinTheta * (vTexCoord.y - 0.5) + 0.5);
	float newY = (sinTheta * (vTexCoord.x - 0.5) + cosTheta * (vTexCoord.y - 0.5) + 0.5);
	vec2 newCoord = vec2(newX, newY);
	
	vec4 color = texture2D(uImage0, newCoord);
    
    gl_FragColor = vec4(color);
}



float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

float scaleClamp(float value, float min, float max, float min2, float max2)
{
	value = min2 + ((value - min) / (max - min)) * (max2 - min2);
	if (max2 > min2)
	{
		value = value < max2 ? value : max2;
		return value > min2 ? value : min2;
	}
	value = value < min2 ? value : min2;
	return value > max2 ? value : max2;
}