//#version 130

//#ifdef GL_ES
//    precision mediump float;
//#endif

varying vec2 vTexCoord;
uniform sampler2D uImage0;
uniform float time;

float rand(vec2 co);
float scaleClamp(float value, float min, float max, float min2, float max2);



void main(void)
{
	vec2 texCoord = vec2(vTexCoord.x, vTexCoord.y);
	vec2 p = -1.0 + 2.0 * texCoord;
	float len = length(p);
	vec2 uv = texCoord + (p / len) * cos(len * 12.0 - time * 4.0) * 0.003;
	vec3 col = texture2D(uImage0, uv).xyz;
	
    gl_FragColor = vec4(col, 1.0);
}



float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

float scaleClamp(float value, float min, float max, float min2, float max2)
{
	value = min2 + ((value - min) / (max - min)) * (max2 - min2);
	if (max2 > min2)
	{
		value = value < max2 ? value : max2;
		return value > min2 ? value : min2;
	}
	value = value < min2 ? value : min2;
	return value > max2 ? value : max2;
}