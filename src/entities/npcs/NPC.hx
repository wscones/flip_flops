package entities.npcs;

import haxepunk.Entity;
import haxepunk.HXP;
import haxepunk.Tween.TweenType;
import haxepunk.graphics.Image;
import sconelib.math.SconeMath;
import sconelib.utils.Structures.Vector2D;

class NPC extends Entity 
{
	private var _img:Image;
	
	private var _directionVector:Vector2D;
	
	private var _rotateClockwise:Bool;
	
	
	public function new(x:Float=0, y:Float=0) 
	{
		super(x, y, null, null);
		
		_img = new Image("graphics/gull.png");
		graphic = _img;
		_img.centerOrigin();
	}
	
	public override function added()
	{
		_rotateClockwise = false;
		
		_directionVector = { x:0.0, y:0.0 };
		
		HXP.alarm(Math.random() * 1.0, beginAlarm, TweenType.OneShot);
	}
	
	public override function update()
	{
		super.update();	
		
		_directionVector = SconeMath.GetAngleOffset(_img.angle);
		x -= _directionVector.x;
		y -= _directionVector.y;
		
		if (x < HXP.width * 0.75)
		{
			_img.angle = Math.random() * 125 + 90;
		}
		else if (x > HXP.width)
		{
			_img.angle = Math.random() * 25;
		}
	}
	
	private function beginAlarm():Void
	{
		HXP.alarm(0.5, adjustAngle, TweenType.Looping);
	}
	
	private function adjustAngle():Void
	{
		if (_rotateClockwise)
		{
			_rotateClockwise = false;
			_img.angle -= 15.0;
		}
		else
		{
			_rotateClockwise = true;
			_img.angle += 15.0;
		}
	}
}