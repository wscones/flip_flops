package entities.npcs;

import com.haxepunk.HXP;

class NPCManager 
{
	private var _npcArray:Array<NPC>;
	
	public function new() 
	{
		_npcArray = new Array<NPC>();
		
		for (i in 0...25) 
		{
			_npcArray.push(new NPC(Math.random() * (HXP.width * 0.1) + (HXP.width * 0.9), Math.random() * -1500.0));
		}
	}	
	
	public function init()
	{
		for (i in 0..._npcArray.length) 
		{
			HXP.scene.add(_npcArray[i]);
		}
	}
}