package entities;

import com.haxepunk.Graphic.ImageType;
import com.haxepunk.graphics.Image;
import flash.geom.Rectangle;

class PoolImage extends Image 
{
	private var _isInUse:Bool;
	
	public function new(?source:ImageType, ?clipRect:Rectangle) 
	{
		super(source, clipRect);
		
		_isInUse = false;
	}
	
	
	public var isInUse(get, set):Bool;
	public function get_isInUse():Bool { return _isInUse; }
	public function set_isInUse(value:Bool):Bool
	{
		_isInUse = value;
		return _isInUse;
	}
}