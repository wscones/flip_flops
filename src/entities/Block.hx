package entities;

import com.haxepunk.graphics.Image;
import com.haxepunk.Entity;
import com.haxepunk.HXP;
import utils.Constants;

class Block extends Entity
{
	private var _img:Image;
	
	private var _direction:String;
	
	private var _angle:Float;
	
	public function new(img:Image, direction:String) 
	{
		super();
		
		setImgAndDirection(img, direction);
		_angle = 0.0;
	}
	
	public function setImgAndDirection(img:Image, direction:String)
	{
		_img = img;
		graphic = _img;
		
		_direction = direction;
	}
	
	public override function update()
	{
		super.update();
	}
	
	
	public var direction(get, null):String;
	public function get_direction():String { return _direction; }
	
	public var angle(get, set):Float;
	public function get_angle():Float { return _angle; }
	public function set_angle(value:Float):Float
	{
		_angle = value;
		_img.angle = _angle;
		return _angle;
	}
}