package entities.menus;
import haxepunk.HXP;
import haxepunk.graphics.Graphiclist;
import haxepunk.graphics.Image;
import haxepunk.math.MathUtil;
import sconelib.math.SconeMath;

class CharacterItem extends MenuItem 
{
	private var _graphicList:Graphiclist;
	private var _img:Image;
	
	public function new(text:String, imgLoc:String) 
	{
		super(text);
		
		_graphicList = new Graphiclist();
		_img = new Image(imgLoc);
		
		_graphicList.add(_img);
		_graphicList.add(_textGraphic);
		
		graphic = _graphicList;
	}
	
	public override function init(onScreenX:Float, onScreenY:Float, offScreenX:Float, offScreenY:Float) 
	{
		super.init(onScreenX, onScreenY, offScreenX, offScreenY);
		
		_img.scale = 0.5;
		_img.alpha = 0.5;
		_img.smooth = false;
		_img.centerOrigin();
		
		_textGraphic.y = _img.y + _img.height * 0.5;
	}
	
	private override function handleSelectState()
	{
		if (_selected)
		{
			if (!SconeMath.ApproxFloat(_img.scale, 1.0, 0.1))
			{
				_img.scale = MathUtil.lerp(_img.scale, 1.0, 0.1);
				_img.alpha = MathUtil.lerp(_img.alpha, 1.0, 0.1);
			}
		}
		else
		{
			if (!SconeMath.ApproxFloat(_img.scale, 0.5, 0.1))
			{
				_img.scale = MathUtil.lerp(_img.scale, 0.5, 0.1);
				_img.alpha = MathUtil.lerp(_img.alpha, 0.5, 0.1);
			}
		}
	}
	
	public override function onSelect()
	{
		super.onSelect();
		
		layer = 0;
	}
	
	public override function onDeselect()
	{
		super.onDeselect();
		
		layer = 1;
	}
	
	public override function onClick()
	{
		super.onClick();
		
		Main.sceneMain.character = _textGraphic.text;
		HXP.scene = Main.sceneMenu;
	}
}