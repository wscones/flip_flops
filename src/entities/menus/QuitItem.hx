package entities.menus;
import entities.menus.MenuItem;
import openfl.system.System;

class QuitItem extends MenuItem
{
	public override function onClick()
	{
		System.exit(0);
	}
}