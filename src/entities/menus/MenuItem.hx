package entities.menus;

import haxepunk.Entity;
import haxepunk.Graphic;
import haxepunk.HXP;
import haxepunk.Mask;
import haxepunk.graphics.text.Text;
import haxepunk.math.MathUtil;
import openfl.geom.Point;
import openfl.text.TextFormatAlign;
import sconelib.math.SconeMath;

class MenuItem extends Entity 
{
	private var _textGraphic:Text;
	
	private var _onScreenPos:Point;
	private var _offScreenPos:Point;
	
	private var _selected:Bool;
	
	public function new(text:String) 
	{
		super();
		
		_textGraphic = new Text(text);
		graphic = _textGraphic;
		
		_onScreenPos = new Point();
		_offScreenPos = new Point();
	}
	
	public function init(onScreenX:Float, onScreenY:Float, offScreenX:Float, offScreenY:Float)
	{
		//init screen positions
		_onScreenPos.x = onScreenX;
		_onScreenPos.y = onScreenY;
		_offScreenPos.x = offScreenX;
		_offScreenPos.y = offScreenY;
		
		x = _offScreenPos.x;
		y = _offScreenPos.y;
		
		//init text
		_textGraphic.font = "font/lowqual.ttf";
		_textGraphic.size = 48;
		_textGraphic.smooth = false;
		_textGraphic.centerOrigin();
		_textGraphic.align = TextFormatAlign.CENTER;
		_textGraphic.color = 0x0000FF;
		
		//init vars
		_selected = false;
	}
	
	public override function added()
	{
		super.added();
	}
	
	public override function update()
	{
		super.update();
		
		handleSelectState();
	}
	
	private function handleSelectState()
	{
		if (_selected)
		{
			if (!SconeMath.ApproxFloat(_textGraphic.scale, 2.0, 0.1))
			{
				_textGraphic.scale = MathUtil.lerp(_textGraphic.scale, 2.0, 0.1);
			}
		}
		else
		{
			if (!SconeMath.ApproxFloat(_textGraphic.scale, 1.0, 0.1))
			{
				_textGraphic.scale = MathUtil.lerp(_textGraphic.scale, 1.0, 0.1);
			}
		}
	}
	
	
	public function onSelect()
	{
		_textGraphic.color = 0xFF0080;
		_selected = true;
	}
	
	public function onDeselect()
	{
		_textGraphic.color = 0x0000FF;
		_selected = false;
	}
	
	public function onClick()
	{
	}
	
	
	public var onScreenPos(get, null):Point;
	public function get_onScreenPos():Point { return _onScreenPos; }
	
	public var offScreenPos(get, null):Point;
	public function get_offScreenPos():Point { return _offScreenPos; }
}