package entities.menus;
import utils.Constants.Scenes;

class SwitchToSelectItem extends MenuItem 
{
	public override function onClick()
	{
		Main.sceneMenu.transitionOut(Scenes.CHARACTER_SELECT);
	}
}