package entities.menus;
import haxepunk.HXP;
import haxepunk.Tween.TweenType;
import haxepunk.tweens.misc.MultiVarTween;
import haxepunk.utils.Ease;
import haxepunk.input.Input;
import haxepunk.input.Key;
import entities.menus.MenuItem;
import utils.Constants.Scenes;
import sconelib.utils.JoystickWrapper;

class MenuManager 
{
	private var _menuItems:Array<MenuItem>;
	private var _menuItemTweens:Array<MultiVarTween>;
	
	private var _didInit:Bool;
	
	private var _selectedItemIndex:Int;
	private var _canChangeSelection:Bool;
	
	private var _sceneToTransitionTo:String;
	
	
	public function new() 
	{
		_menuItems = new Array<MenuItem>();
		_menuItemTweens = new Array<MultiVarTween>();
		
		_didInit = false;
		
		populateMenu();
		
		_selectedItemIndex = 0;
		_canChangeSelection = true;
		
		_sceneToTransitionTo = "";
		
		Input.define("up", [Key.UP]);
		Input.define("down", [Key.DOWN]);
		Input.define("left", [Key.LEFT]);
		Input.define("right", [Key.RIGHT]);
		Input.define("select", [Key.SPACE]);
	}
	
	/**
	 * Populate the menu with child classes of MenuItem
	 */
	private function populateMenu()
	{
		var numItems:Int = 3;
		for (i in 0...numItems) 
		{
			if (i == 0)
			{
				_menuItems.push(new StartItem("Start"));
			}
			else if (i == numItems - 1)
			{
				_menuItems.push(new QuitItem("Quit"));
			}
			else
			{
				_menuItems.push(new SwitchToSelectItem("Character Select"));
			}
			
			var tween:MultiVarTween = new MultiVarTween(TweenType.OneShot);
			tween.onComplete.bind(menuItemDoneTween);
			_menuItemTweens.push(tween);
		}
	}
	
	public function begin()
	{
		for (i in 0..._menuItems.length)
		{
			_menuItemTweens[i].tween(_menuItems[i], {x:_menuItems[i].onScreenPos.x, y:_menuItems[i].onScreenPos.y}, 0.6 * (i + 1), Ease.backOut);
		}
	}
	
	public function end()
	{
		for (i in 0..._menuItems.length)
		{
			_menuItemTweens[i].tween(_menuItems[i], {x:_menuItems[i].offScreenPos.x, y:_menuItems[i].offScreenPos.y}, 0.6 * (i + 1), Ease.backIn);
			
		}
		
		HXP.alarm(0.6 * (_menuItems.length), transitionOut, TweenType.OneShot);
	}
	
	/**
	 * After tweening all of the MenuItems off of the screen, change the scene
	 */
	private function transitionOut():Void
	{
		if (_sceneToTransitionTo == Scenes.GAME)
		{
			HXP.scene = Main.sceneMain;
		}
		else if (_sceneToTransitionTo == Scenes.CHARACTER_SELECT)
		{
			HXP.scene = Main.sceneCharacterSelect;
		}
	}
	
	
	public function init()
	{
		//init menu items
		initMenuItems();
		
		if (!_didInit)
		{
			_didInit = true;
			for (i in 0..._menuItems.length)
			{
				HXP.scene.add(_menuItems[i]);
			}
			
			//init tweens
			for (j in 0..._menuItemTweens.length) 
			{
				HXP.scene.addTween(_menuItemTweens[j]);
			}
		}
		
		selectItem(_selectedItemIndex);
		selectItem(_selectedItemIndex);
	}
	
	private function initMenuItems()
	{
		_menuItems[0].init(HXP.halfWidth, HXP.halfHeight * 0.8, HXP.halfWidth * 4.0, HXP.halfHeight);
		_menuItems[1].init(HXP.halfWidth, HXP.halfHeight * 1.2, HXP.halfWidth * 4.0, HXP.halfHeight * 1.25);
		_menuItems[2].init(HXP.halfWidth, HXP.halfHeight * 1.6, HXP.halfWidth * 4.0, HXP.halfHeight * 1.5);
	}
	
	
	public function update()
	{
		handleInput(true);	
	}
	
	private function handleInput(isVertical:Bool, waitForItemTween:Bool = false)
	{
		var prev:String = "left";
		var next:String = "right";
		var axis:Int = 0;
		
		if (isVertical)
		{
			prev = "up";
			next = "down";
			axis = 1;
		}
		
		if (_canChangeSelection)
		{
			if (Input.pressed(prev) || JoystickWrapper.getAxisPressed(0, axis, false))
			{
				if (waitForItemTween)
				{
					_canChangeSelection = false;
				}
				
				deselectItem(_selectedItemIndex);
				
				if (_selectedItemIndex > 0)
				{
					_selectedItemIndex--;
				}
				else
				{
					_selectedItemIndex = _menuItems.length - 1;
				}
				
				selectItem(_selectedItemIndex);
			}
			else if (Input.pressed(next) || JoystickWrapper.getAxisPressed(0, axis, true))
			{
				if (waitForItemTween)
				{
					_canChangeSelection = false;
				}
				
				deselectItem(_selectedItemIndex);
				
				if (_selectedItemIndex < _menuItems.length - 1)
				{
					_selectedItemIndex++;
				}
				else
				{
					_selectedItemIndex = 0;
				}
				
				selectItem(_selectedItemIndex);
			}
		}
		
		if (Input.pressed("select") || JoystickWrapper.pressed(0, 0))
		{
			clickItem(_selectedItemIndex);
		}
	}
	
	private function menuItemDoneTween():Void
	{
		_canChangeSelection = true;
	}
	
	private function selectItem(index:Int)
	{
		_menuItems[index].onSelect();
	}
	
	private function deselectItem(index:Int)
	{
		_menuItems[index].onDeselect();
	}
	
	private function clickItem(index:Int)
	{
		_menuItems[index].onClick();
	}
	
	
	
	public var sceneToTransitionTo(get, set):String;
	public function get_sceneToTransitionTo():String { return _sceneToTransitionTo; }
	public function set_sceneToTransitionTo(value:String):String
	{
		_sceneToTransitionTo = value;
		return _sceneToTransitionTo;
	}
}