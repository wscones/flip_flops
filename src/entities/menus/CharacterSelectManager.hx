package entities.menus;
import haxepunk.HXP;
import haxepunk.tweens.misc.MultiVarTween;
import haxepunk.Tween.TweenType;
import haxepunk.utils.Ease;
import haxepunk.input.Input;
import haxepunk.input.Gamepad;
import utils.Constants.Characters;

class CharacterSelectManager extends MenuManager 
{
	public function new() 
	{
		super();
	}
	
	private override function populateMenu() 
	{
		var numItems:Int = 3;
		for (i in 0...numItems) 
		{
			if (i == 0)
			{
				_menuItems.push(new CharacterItem(Characters.FLIP_FLOPS, "graphics/flop_duo.png"));
			}
			else if (i == 1)
			{
				_menuItems.push(new CharacterItem(Characters.SNEAKERS, "graphics/sneakers_duo.png"));
			}
			else if (i == 2)
			{
				_menuItems.push(new CharacterItem(Characters.CLOGS, "graphics/clog_duo.png"));
			}
			
			var tween = new MultiVarTween(TweenType.OneShot);
			tween.onComplete.bind(menuItemDoneTween);
			_menuItemTweens.push(tween);
		}
	}
	
	
	public override function begin()
	{
		selectItem(_selectedItemIndex);
	}
	
	private override function initMenuItems() 
	{
		_menuItems[0].init(HXP.halfWidth, HXP.height * 0.4, HXP.width * 0.25, HXP.height * 0.75);
		_menuItems[1].init(HXP.halfWidth, HXP.height * 0.4, HXP.width * 0.5, HXP.height * 0.75);
		_menuItems[2].init(HXP.halfWidth, HXP.height * 0.4, HXP.width * 0.75, HXP.height * 0.75);
	}
	
	public override function update()
	{
		handleInput(false, true);
		
		if (Gamepad.gamepadCount > 0 && Gamepad.gamepad(0).pressed(1))
		{
			HXP.scene = Main.sceneMenu;
		}
	}
	
	
	private override function selectItem(index:Int)
	{
		super.selectItem(index);
		
		_menuItemTweens[index].tween(_menuItems[index], {x:_menuItems[index].onScreenPos.x, y:_menuItems[index].onScreenPos.y}, 0.7, Ease.circOut);
	}
	
	private override function deselectItem(index:Int)
	{
		super.deselectItem(index);
		
		_menuItemTweens[index].tween(_menuItems[index], {x:_menuItems[index].offScreenPos.x, y:_menuItems[index].offScreenPos.y}, 0.7, Ease.circIn);
	}
}