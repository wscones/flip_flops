package entities.menus;
import com.haxepunk.HXP;
import entities.menus.MenuItem;
import utils.Constants.Scenes;

class StartItem extends MenuItem
{
	public override function onClick()
	{
		Main.sceneMenu.transitionOut(Scenes.GAME);
	}
}