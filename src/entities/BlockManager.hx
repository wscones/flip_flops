package entities;
import haxepunk.graphics.Image;
import haxepunk.HXP;
import haxepunk.utils.Draw;
import haxepunk.input.Input;
import haxepunk.input.Key;
import haxepunk.input.Gamepad;
import haxepunk.math.Random;
import haxepunk.math.MathUtil;
import openfl.geom.Point;
import utils.Constants;
import sconelib.math.SconeMath;
import sconelib.utils.JoystickWrapper;

class BlockManager
{
	//block stuff
	private var _maxNumBlocks:Int;
	
	private var _blockImgLeft:Image;
	private var _blockImgRight:Image;
	private var _blockImgUp:Image;
	private var _blockImgDown:Image;
	
	private var _imgWidth:Float;
	private var _imgHeight:Float;
	
	private var _blockArray:Array<Block>;
	private var _tempBlockArray:Array<Block>;
	
	//logic
	private var _leftFootArray:Array<Bool>;
	private var _rightFootArray:Array<Bool>;
	private var _currBlock:Int;
	
	private var _leftFootPos:Point;
	private var _rightFootPos:Point;
	
	//flops
	private var _player:Player;
	
	private var _leftFoot:Image;
	private var _rightFoot:Image;
	
	private var _leftFootMap:Map<String, Image>;
	private var _rightFootMap:Map<String, Image>;
	
	private var _speedCounter:Float;
	
	private var _screenAngle:Float;
	
	
	public function new(maxNumBlocks:Int) 
	{
		_maxNumBlocks = maxNumBlocks;
		
		_blockImgLeft = new Image("graphics/block_left.png");
		_blockImgRight = new Image("graphics/block_right.png");
		_blockImgUp = new Image("graphics/block_up.png");
		_blockImgDown = new Image("graphics/block_down.png");
		
		_player = new Player();
		
		_leftFootMap = new Map<String, Image>();
		_rightFootMap = new Map<String, Image>();
		for (j in 0...Characters.characterArray.length) 
		{
			var leftPath:String = Characters.characterLeftImgPaths[Characters.characterArray[j]];
			var rightPath:String  = Characters.characterRightImgPaths[Characters.characterArray[j]];
			var left:Image = new Image(leftPath);
			var right:Image = new Image(rightPath);
			left.centerOO();
			right.centerOO();
			left.alpha = 0;
			right.alpha = 0;
			
			_leftFootMap.set(Characters.characterArray[j], left);
			_rightFootMap.set(Characters.characterArray[j], right);
		}
		_leftFoot = _leftFootMap[Characters.characterArray[0]];
		_rightFoot = _rightFootMap[Characters.characterArray[0]];
		
		_blockImgLeft.centerOrigin();
		_blockImgRight.centerOrigin();
		_blockImgUp.centerOrigin();
		_blockImgDown.centerOrigin();
		_blockImgLeft.alpha = 0;
		_blockImgRight.alpha = 0;
		_blockImgUp.alpha = 0;
		_blockImgDown.alpha = 0;
		
		scaleBlockImages();
		
		_blockArray = new Array<Block>();
		_tempBlockArray = new Array<Block>();
		_leftFootArray = new Array<Bool>();
		_rightFootArray = new Array<Bool>();
		
		_leftFootPos = new Point();
		_rightFootPos = new Point();
		
		for (i in 0..._maxNumBlocks) 
		{
			var randNum:Int = Random.randInt(4);
			var currImg:Image = HXP.choose(_blockImgLeft, _blockImgRight, _blockImgUp, _blockImgDown);
			var currDirection:String = "";
			switch (randNum)
			{
			case 0:
				currImg = _blockImgLeft;
				currDirection = Constants.LEFT;
			case 1:
				currImg = _blockImgRight;
				currDirection = Constants.RIGHT;
			case 2:
				currImg = _blockImgUp;
				currDirection = Constants.UP;
			case 3:
				currImg = _blockImgDown;
				currDirection = Constants.DOWN;
			default:
			}
			
			_blockArray.push(new Block(currImg, currDirection));
			_leftFootArray.push(false);
			_rightFootArray.push(false);
		}
	}
	
	private function scaleBlockImages()
	{
		//scale blocks
		var imgScaleX:Float = cast(HXP.width / _blockImgLeft.width, Float);
		var imgScaleY:Float = cast((HXP.height / _maxNumBlocks) / _blockImgLeft.height, Float);
		_imgWidth = _blockImgLeft.width * imgScaleX;
		_imgHeight = _blockImgLeft.height * imgScaleY;
		
		_blockImgLeft.scaleX = imgScaleX;
		_blockImgLeft.scaleY = imgScaleY;
		
		_blockImgRight.scaleX = imgScaleX;
		_blockImgRight.scaleY = imgScaleY;
		
		_blockImgUp.scaleX = imgScaleX;
		_blockImgUp.scaleY = imgScaleY;
		
		_blockImgDown.scaleX = imgScaleX;
		_blockImgDown.scaleY = imgScaleY;
		
		//scale feet
		var footScaleY:Float = 1.0;
		
		for (key in _leftFootMap.keys()) 
		{
			footScaleY = _imgHeight / 	_leftFootMap[key].height;
			_leftFootMap[key].scale = footScaleY;
		}
		for (key in _rightFootMap.keys()) 
		{
			footScaleY = _imgHeight / _rightFootMap[key].height;
			_rightFootMap[key].scale = footScaleY;
		}
	}
	
	public function setFeet(characterStr:String)
	{
		if (_leftFoot != null && _rightFoot != null)
		{
			_leftFoot.alpha = 0;
			_rightFoot.alpha = 0;
		}
		
		_leftFoot = _leftFootMap[characterStr];
		_rightFoot = _rightFootMap[characterStr];
		
		_leftFoot.alpha = 1;
		_rightFoot.alpha = 1;
	}
	
	public function init()
	{
		//init block array
		_blockArray[0].x = HXP.halfWidth;
		_blockArray[0].y = HXP.height - _imgHeight / 2.0;
		HXP.scene.add(_blockArray[0]);
		for (i in 1..._blockArray.length) 
		{
			_blockArray[i].x = HXP.halfWidth;
			_blockArray[i].y = _blockArray[i - 1].y - _imgHeight;
			HXP.scene.add(_blockArray[i]);
		}
		
		//init logic
		_currBlock = 0;
		
		//init feet
		setLeftFootPos(_currBlock);
		setRightFootPos(_currBlock);
		
		for (key in _leftFootMap.keys()) 
		{
			HXP.scene.addGraphic(_leftFootMap[key]);
		}
		for (key in _rightFootMap.keys()) 
		{
			HXP.scene.addGraphic(_rightFootMap[key]);
		}
		
		_player.init(_leftFootPos.x, _rightFootPos.x, _leftFootPos.y);
		
		_speedCounter = 0.7;
		
		_screenAngle = 0.0;
		
		//init input
		Input.define("left", [Key.LEFT]);
		Input.define("right", [Key.RIGHT]);
		Input.define("up", [Key.UP]);
		Input.define("down", [Key.DOWN]);
	}
	
	public function update()
	{
		lerpImgAlphas();
		
		_leftFoot.x = _leftFootPos.x;
		_leftFoot.y = _leftFootPos.y;
		_rightFoot.x = _rightFootPos.x;
		_rightFoot.y = _rightFootPos.y;
		_player.update(_leftFootPos, _rightFootPos);
		
		if (_currBlock != _blockArray.length - 1)
		{
			if (_speedCounter > 0.1)
			{
				_speedCounter -= 0.003;
			}
			
			checkForFootsteps(_currBlock + 1);
			
			//for (i in 0..._blockArray.length) 
			//{
			//	_blockArray[i].x += Math.random() * 2 - 1;
			//}
			
			if (!SconeMath.ApproxFloat(HXP.camera.x, _blockArray[_currBlock].x - HXP.halfWidth, 0.1))
			{
				HXP.camera.x = MathUtil.lerp(HXP.camera.x, _blockArray[_currBlock].x - HXP.halfWidth, 0.1);
			}
			if (!SconeMath.ApproxFloat(HXP.camera.y, _blockArray[_currBlock].y - HXP.halfHeight, 0.1))
			{
				HXP.camera.y = MathUtil.lerp(HXP.camera.y, _blockArray[_currBlock].y - HXP.halfHeight, 0.1);
			}
			//if (!SconeMath.ApproxFloat(_screenAngle, _blockArray[_currBlock].angle * (Math.PI / 180), 0.01))
			//{
			//	_screenAngle = HXP.lerp(_screenAngle, _blockArray[_currBlock].angle * (Math.PI / 180), 0.01);
			//}
			//Main.rotate.setUniform("theta", _screenAngle);
		}
		else
		{
			resetBlocks();
			_speedCounter = 0.7;
		}
	}
	
	private function lerpImgAlphas() 
	{
		if (_blockImgLeft.alpha != 1)
		{
			_blockImgLeft.alpha = MathUtil.lerp(_blockImgLeft.alpha, 1, 0.1);
		}
		if (_blockImgRight.alpha != 1)
		{
			_blockImgRight.alpha = MathUtil.lerp(_blockImgRight.alpha, 1, 0.1);
		}
		if (_blockImgUp.alpha != 1)
		{
			_blockImgUp.alpha = MathUtil.lerp(_blockImgUp.alpha, 1, 0.1);
		}
		if (_blockImgDown.alpha != 1)
		{
			_blockImgDown.alpha = MathUtil.lerp(_blockImgDown.alpha, 1, 0.1);
		}
	}
	
	private function resetBlocks()
	{
		_blockImgLeft.alpha = 0;
		_blockImgRight.alpha = 0;
		_blockImgUp.alpha = 0;
		_blockImgDown.alpha = 0;
		
		//put all blocks into the temp array so we randomize them
		var numBlocksMinusOne:Int = _maxNumBlocks - 1;
		for (i in 0...numBlocksMinusOne)
		{
			_tempBlockArray.push(_blockArray.shift());
		}
		
		//randomize the direction
		for (j in 0...numBlocksMinusOne) 
		{
			var randNum:Int = Random.randInt(4);
			switch (randNum)
			{
			case 0:
				_tempBlockArray[j].setImgAndDirection(_blockImgLeft, Constants.LEFT);
			case 1:
				_tempBlockArray[j].setImgAndDirection(_blockImgRight, Constants.RIGHT);
			case 2:
				_tempBlockArray[j].setImgAndDirection(_blockImgUp, Constants.UP);
			case 3:
				_tempBlockArray[j].setImgAndDirection(_blockImgDown, Constants.DOWN);
			default:
			}
		}
		
		//put them back in the block array
		while (_tempBlockArray.length > 0)
		{
			_blockArray.push(_tempBlockArray.pop());
		}
		
		//reset positions
		for (k in 1..._blockArray.length) 
		{
			_blockArray[k].x = HXP.halfWidth;
			_blockArray[k].y = _blockArray[k - 1].y - _imgHeight;
			//_blockArray[k].angle += 5;
		}
		
		for (l in 0..._leftFootArray.length) 
		{
			_leftFootArray[l] = false;
		}
		for (m in 0..._rightFootArray.length) 
		{
			_rightFootArray[m] = false;
		}
		
		_currBlock = 0;
		setLeftFootPos(_currBlock);
		setRightFootPos(_currBlock);
	}
	
	private function checkForFootsteps(index:Int)
	{
		if (!_leftFootArray[index] || !_rightFootArray[index])
		{
			if (_leftFootArray[index])
			{
				setLeftFootPos(index);
				setRightFootPos(index - 1);
			}
			else
			{
				setLeftFootPos(index - 1);
				setRightFootPos(index - 1);
			}
			
			if (correctKeyWasPressed(index))
			{
				if (!_leftFootArray[index])
				{
					_leftFootArray[index] = true;
					setLeftFootPos(index);
				}
				else if (!_rightFootArray[index])
				{
					_rightFootArray[index] = true;
					setRightFootPos(index);
				}
			}
		}
		else
		{
			_currBlock++;
		}
	}
	
	private function correctKeyWasPressed(index:Int):Bool
	{		
		if (Gamepad.gamepadCount > 0 && Gamepad.gamepad(0).connected)
		{
			return correctJoystickButtonPressed(index);
		}
		else
		{
			if (Input.pressed(_blockArray[index].direction))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		return false;
	}
	
	private function correctJoystickButtonPressed(index:Int):Bool
	{
		if (_blockArray[index].direction == Constants.LEFT)
		{
			if (JoystickWrapper.pressed(0, 2) || JoystickWrapper.getAxisPressed(0, 0, false))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if (_blockArray[index].direction == Constants.RIGHT)
		{
			if (JoystickWrapper.pressed(0, 1) || JoystickWrapper.getAxisPressed(0, 0, true))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if (_blockArray[index].direction == Constants.UP)
		{
			if (JoystickWrapper.pressed(0, 3) || JoystickWrapper.getAxisPressed(0, 1, false))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if (_blockArray[index].direction == Constants.DOWN)
		{
			if (JoystickWrapper.pressed(0, 0) || JoystickWrapper.getAxisPressed(0, 1, true))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		return false;
	}
	
	private function setLeftFootPos(index:Int)
	{
		_leftFootPos.x = _blockArray[index].x - _imgWidth * 0.07;// + _imgWidth / 3.0;
		_leftFootPos.y = _blockArray[index].y;// + _imgHeight / 2.0;
	}

	private function setRightFootPos(index:Int)
	{
		_rightFootPos.x = _blockArray[index].x + _imgWidth * 0.07;//+ _imgWidth / 1.5;
		_rightFootPos.y = _blockArray[index].y;// + _imgHeight / 2.0;
	}
}