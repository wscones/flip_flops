package entities;

import com.haxepunk.graphics.Image;
import entities.PoolImage;
import scenes.MainScene;
import utils.Constants;

class BlockPool 
{
	private var _blockImagesLeft:Array<PoolImage>;
	private var _blockImagesRight:Array<PoolImage>;
	private var _blockImagesUp:Array<PoolImage>;
	private var _blockImagesDown:Array<PoolImage>;
	
	public function new() 
	{
		for (i in 0...MainScene.maxNumBlocks) 
		{
			_blockImagesLeft.push(new PoolImage("graphics/block_left.png"));
			_blockImagesRight.push(new PoolImage("graphics/block_right.png"));
			_blockImagesUp.push(new PoolImage("graphics/block_up.png"));
			_blockImagesDown.push(new PoolImage("graphics/block_down.png"));
		}
	}
	
	public function getFreeBlock(direction:String):Image
	{
		if (direction == Constants.LEFT)
		{
			return findFreeBlock(_blockImagesLeft);
		}
		else if (direction == Constants.RIGHT)
		{
			return findFreeBlock(_blockImagesRight);
		}
		else if (direction == Constants.UP)
		{
			return findFreeBlock(_blockImagesUp);
		}
		else if (direction == Constants.DOWN)
		{
			return findFreeBlock(_blockImagesDown);
		}
	}
	
	private function findFreeBlock(blockArray:Array<PoolImage>():PoolImage
	{
		for (i in 0...blockArray.length) 
			{
				if (!blockArray[i].isInUse)
				{
					blockArray[i].isInUse = true;
					return blockArray[i];
				}
			}
	}
}