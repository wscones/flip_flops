package entities;
import haxepunk.HXP;
import haxepunk.graphics.Image;
import haxepunk.math.MathUtil;
import openfl.geom.Point;
import sconelib.math.SconeMath;

class Player 
{
	private var _leftLeg:Array<Image>;
	private var _rightLeg:Array<Image>;
	
	private var _torso:Image;
	
	public function new() 
	{
		_leftLeg = new Array<Image>();
		_rightLeg = new Array<Image>();
		
		for (i in 0...5) 
		{
			_leftLeg.push(Image.createCircle(16 + i, 0x0000FF, 0.2 * i + 0.2));
			_rightLeg.push(Image.createCircle(16 + i, 0x0000FF, 0.2 * i + 0.2));
		}
		
		_torso = Image.createRect(128, 48, 0x0000FF, 0.2);
	}
	
	public function init(leftFootX:Float, rightFootX:Float, footY:Float)
	{
		for (i in 0..._leftLeg.length) 
		{
			_leftLeg[i].centerOrigin();
			_rightLeg[i].centerOrigin();
			
			_leftLeg[i].x = leftFootX;
			_leftLeg[i].y = footY;
			_rightLeg[i].x = rightFootX;
			_rightLeg[i].y = footY;
			
			_torso.centerOrigin();
			_torso.x = (leftFootX + rightFootX) / 2.0;
			_torso.y = footY;
			
			HXP.scene.addGraphic(_leftLeg[i]);
			HXP.scene.addGraphic(_rightLeg[i]);
			HXP.scene.addGraphic(_torso);
		}
	}
	
	public function update(leftFootPos:Point, rightFootPos:Point)
	{
		if (SconeMath.ApproxFloat(leftFootPos.y, rightFootPos.y, 0.2))
		{
			_torso.y = rightFootPos.y + _rightLeg[0].height * 0.4;
			_torso.angle = MathUtil.lerp(_torso.angle, 0.0, 0.2);
			
			_leftLeg[0].x = MathUtil.lerp(_leftLeg[0].x, leftFootPos.x, 0.2);
			_leftLeg[0].y = leftFootPos.y + _leftLeg[0].height * 0.4;
			for (i in 1..._leftLeg.length) 
			{
				if (_leftLeg[i].y != _leftLeg[i - 1].y)
				{
					_leftLeg[i].y = MathUtil.lerp(_leftLeg[i].y, _leftLeg[i - 1].y, 0.2);
				}
			}
			
			_rightLeg[0].x = MathUtil.lerp(_rightLeg[0].x, rightFootPos.x, 0.2);
			_rightLeg[0].y = rightFootPos.y + _rightLeg[0].height * 0.4;
			for (i in 1..._rightLeg.length) 
			{
				if (_rightLeg[i].y != _rightLeg[i - 1].y)
				{
					_rightLeg[i].y = MathUtil.lerp(_rightLeg[i].y, _rightLeg[i - 1].y, 0.4);
				}
			}
		}
		else
		{
			_torso.y = MathUtil.lerp(_torso.y, rightFootPos.y - _torso.height / 2.0, 0.2);
			_torso.angle = MathUtil.lerp(_torso.angle, -22.5, 0.2);
			
			_leftLeg[0].x = MathUtil.lerp(_leftLeg[0].x, leftFootPos.x, 0.2);
			_leftLeg[0].y = leftFootPos.y + _leftLeg[0].height * 0.4;
			for (i in 1..._leftLeg.length) 
			{
				if (_leftLeg[i - 1].y < _leftLeg[i].y - _leftLeg[i].height / 3.0)
				{
					_leftLeg[i].y = MathUtil.lerp(_leftLeg[i].y, _leftLeg[i].y - _leftLeg[i].height / 3.0, 0.7);
				}
			}
			
		}
	}
}