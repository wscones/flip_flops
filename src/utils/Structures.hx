package utils;

typedef Vector2D =
{
	var x:Float;
	var y:Float;
}

/*typedef ConvoInfo =
{
	var sentenceDone:Bool;
	var shouldStart:Bool;
	var myTurn:Bool;
}*/

class ConvoInfo
{
	public var sentenceDone:Bool;
	public var shouldStart:Bool;
	public var myTurn:Bool;
	public var firstSpeaker:Bool;
	public var isPlayer:Bool;
	
	public var convoLength:Int;
	
	public function new()
	{
		init();
	}
	
	public function init()
	{
		sentenceDone = false;
		shouldStart = false;
		myTurn = false;
		firstSpeaker = false;
		isPlayer = false;
		
		convoLength = 0;
	}
}