package utils;

class Constants
{
	static public var LEFT:String = "left";
	static public var RIGHT:String = "right";
	static public var UP:String = "up";
	static public var DOWN:String = "down";
	static public var NONE:String = "none";
}

class Types
{
	static public var PLAYER:String = "player";
	static public var NPC:String = "npc";
}

class Scenes
{
	static public var GAME:String = "game";
	static public var MENU:String = "menu";
	static public var CHARACTER_SELECT:String = "character_select";
}

class Characters
{
	static public var FLIP_FLOPS:String = "flips";
	static public var SNEAKERS:String = "sneaks";
	static public var CLOGS:String = "clogs";
	
	static public var characterArray:Array<String> = 
	[
		FLIP_FLOPS,
		SNEAKERS,
		CLOGS
	];
	
	static public var characterLeftImgPaths:Map<String, String> =
	[
		FLIP_FLOPS => "graphics/flop_left.png",
		SNEAKERS => "graphics/sneakers_left.png",
		CLOGS => "graphics/clog_left.png"
	];
	
	static public var characterRightImgPaths:Map<String, String> =
	[
		FLIP_FLOPS => "graphics/flop_right.png",
		SNEAKERS => "graphics/sneakers_right.png",
		CLOGS => "graphics/clog_right.png"
	];
}