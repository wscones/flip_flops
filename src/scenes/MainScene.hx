package scenes;

import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.graphics.tile.Backdrop;
import entities.Block;
import entities.BlockManager;
import entities.npcs.NPCManager;
import utils.Constants;
import sconelib.TimedScene;

class MainScene extends TimedScene
{
	public static var maxNumBlocks = 5;
	
	
	private var _bg:Backdrop;
	
	private var _blockManager:BlockManager;
	private var _npcManager:NPCManager;
	
	private var _character:String;
	
	public function new()
	{
		super();
		
		_bg = new Backdrop("graphics/bg_sidewalk.png", true, true);
		_bg.x = HXP.width * 0.03;
		
		_blockManager = new BlockManager(maxNumBlocks);
		_npcManager = new NPCManager();
		
		_character = Characters.FLIP_FLOPS;
	}
	
	public override function begin()
	{
		super.begin();
		
		addGraphic(_bg);
		
		init();
		_blockManager.setFeet(_character);
	}
	
	public override function end()
	{
	}
	
	private function init()
	{
		//init block manager
		_blockManager.init();
		
		//init npc manager
		_npcManager.init();
		
		//HXP.screen.originX = Std.int(HXP.halfWidth);
		//HXP.screen.originY = Std.int(HXP.halfHeight);
	}
	
	public override function update()
	{
		super.update();
		
		_blockManager.update();
	}
	
	public override function fixedUpdate()
	{
	}
	
	
	public var character(get, set):String;
	public function get_character():String { return _character; }
	public function set_character(value:String):String
	{
		_character = value;
		return _character;
	}
}