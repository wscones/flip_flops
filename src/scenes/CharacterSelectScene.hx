package scenes;

import com.haxepunk.Scene;
import entities.menus.CharacterSelectManager;
import entities.menus.MenuManager;
import sconelib.TimedScene;

class CharacterSelectScene extends TimedScene 
{
	private var _characterManager:CharacterSelectManager;
	
	public function new()
	{
		super();
		
		_characterManager = new CharacterSelectManager();
	}
	
	public override function begin()
	{
		super.begin();
		
		_characterManager.init();
		_characterManager.begin();
	}
	
	public override function end()
	{
	}
	
	public override function update()
	{
		super.update();
		
		_characterManager.update();
	}
	
	public override function fixedUpdate()
	{
	}
	
	
	public function transitionOut()
	{
		_characterManager.end();
	}
}