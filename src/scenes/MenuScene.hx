package scenes;

import haxepunk.Scene;
import haxepunk.graphics.shader.SceneShader;
import entities.menus.MenuManager;
import sconelib.TimedScene;
import sconelib.time.Time;

class MenuScene extends TimedScene 
{
	private var _menuManager:MenuManager;
	
	private var wave:SceneShader;
	private var fuzz:SceneShader;
	private var rotate:SceneShader;
	
	public function new()
	{
		super();
		
		_menuManager = new MenuManager();
	}
	
	public override function begin()
	{
		super.begin();
		
		_menuManager.init();
		_menuManager.begin();
		
		/*//shaders
		wave = SceneShader.fromAsset("shaders/wave.frag");
		fuzz = SceneShader.fromAsset("shaders/fuzz.frag");
		rotate = SceneShader.fromAsset("shaders/rotate.frag");
		wave.setUniform("time", 1.0);
		rotate.setUniform("theta", 0.0);
		//wave.enable(fuzz);
		//fuzz.enable(rotate);
		//rotate.enable();
		shaders = [wave, fuzz, rotate];*/
	}
	
	public override function end()
	{
	}
	
	public override function update()
	{
		super.update();
		
		_menuManager.update();
		//wave.setUniform("time", Time.totalTime);
	}
	
	public override function fixedUpdate()
	{
	}
	
	
	public function transitionOut(sceneToTransitionTo:String)
	{
		_menuManager.sceneToTransitionTo = sceneToTransitionTo;
		_menuManager.end();
	}
}