import haxepunk.Engine;
import haxepunk.HXP;
import haxepunk.debug.Console;
import scenes.CharacterSelectScene;
import scenes.MainScene;
import scenes.MenuScene;

class Main extends Engine
{
	public static var sceneMain:MainScene;
	public static var sceneMenu:MenuScene;
	public static var sceneCharacterSelect:CharacterSelectScene;
	
	
	override public function init()
	{
#if debug
		Console.enable();
#end
		
		//scenes
		sceneMain = new MainScene();
		sceneMenu = new MenuScene();
		sceneCharacterSelect = new CharacterSelectScene();
		
		HXP.scene = sceneMenu;
	}

	/*override public function resize()
	{
		super.resize();
		if (wave != null)
		{
			wave.rebuild();
		}
		if (fuzz != null)
		{
			fuzz.rebuild();
		}
		if (rotate != null)
		{
			rotate.rebuild();
		}
	}
	
	override public function render()
	{
		wave.capture();
		super.render();
	}*/
	
	public static function main() { new Main(); }

}