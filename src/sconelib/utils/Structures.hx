package sconelib.utils;

typedef Vector2D =
{
	var x:Float;
	var y:Float;
}