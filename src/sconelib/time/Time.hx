package sconelib.time;

import com.haxepunk.HXP;

class Time 
{
	/**
	 * Time since the last tick.
	 */
	public static var time:Float = 0.0;
	
	/**
	 * How much the totalTime should be incremented every tick (in this case, 1 second).
	 */
	public static var incrementAmount:Float = 1.0;
	
	/**
	 * Total time since the game began (in seconds). Only increments on every tick (1 second).
	 */
	public static var totalFixedTime:Float = 0.0;
	
	/**
	 * Total time since the game began (in seconds). Increments on every frame.
	 */
	public static var totalTime:Float = 0.0;
	
	/**
	 * Increment the totalTime by the deltaTime.
	 */
	public static function increment(deltaTime:Float)
	{
		totalTime += deltaTime;
	}
	
	/**
	 * Increment the totalFixedTime by the incrementAmount.
	 */
	public static function incrementFixed()
	{
		totalFixedTime += incrementAmount;
	}
}